#

![branding](images/branding.png)

## ITS103F Project Assignment

![screenshot](images/screenshot.jpg)

[latest build](https://its103f.gitlab.io/project-assignment/coverPage.html)

some assets((visual and audio)) in the game are not opensource assets and should be treated as placeholders and are not protected by the linsese, to rebulish the game one should replace all those assets

| todo | done |
| ------ | ------ |
| better story telling | 0%, fuck it |
| implement a score board (core requirement) | 99%, it is useable but ugly |
| improve the random word generation | 50%, currently stoped all developments due to time constring |
| bug testing | inprogress |
| cleanup code for final realese| 30% |
| replace the assets | 0% |
