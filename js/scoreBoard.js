//score board
var scoreRecord, playerId, nameArray, scoreArray;

$(document).ready(function() {
    if(localStorage.getItem("totalPlayerNumber") == undefined){
        localStorage.setItem("totalPlayerNumber",0);
    }
})

function storeLocal(name,score){
    playerId = parseInt(localStorage.getItem("totalPlayerNumber"))+ 1;
    localStorage.setItem("totalPlayerNumber",playerId)
    storedStr = name +" "+ score;
    localStorage.setItem(playerId, storedStr)
}

function scoreBoard(){
    nameArray = [];
    scoreArray = [];
    //split local record into 2 arrays
    for(playerId = 1;playerId <= localStorage.getItem("totalPlayerNumber");playerId += 1){
        scoreRecord = localStorage.getItem(playerId).split(" ");
        var name = scoreRecord[0];
        var score = scoreRecord[1];
        nameArray.push(name);
        scoreArray.push(score);
    }
    //combine the arrays
    var list = [];
    for(var i = 0; i < nameArray.length; i += 1){
        list.push({'name': nameArray[i], 'score': parseInt(scoreArray[i])});
    }
    //sort
    list.sort(function(a, b) {
        return ((a.score > b.score) ? -1 : ((a.score == b.score) ? 0 : 1));

    });
    //separate them back out
    for(var i = 0; i < list.length; i += 1) {
        nameArray[i] = list[i].name;
        scoreArray[i] = list[i].score;
    }
    //return output;
    var output = " name | wpm \t\n";
    for(playerId=1;playerId<=localStorage.getItem("totalPlayerNumber");playerId+=1){
        output += " "+nameArray[playerId-1] +": "+ scoreArray[playerId-1]+" \n";
    }
    alert(output);
}

function resetRecord(){
    //does not really clear all records but reset the total player number, so the score board does not show the other scores
    localStorage.setItem("totalPlayerNumber",0);
}