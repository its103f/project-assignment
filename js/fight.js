//fighting mechanics
var inGame = false;
var gameStatus = false;
var bossHealth, playerHealth, dmgDealt, dmgTaken, hearts;

function playerHealthBar(){
    //todo take dmg
    if(dmgTaken > 0){
        playerHealth -= dmgTaken;
    }

    hearts = "";
    for(i = playerHealth;i > 0;i -= 1){
        hearts += "❤️" ;
    }
    document.getElementById("playerHealthBar").innerHTML = hearts;

    if(playerHealth <= 0){
        gameOver();
    }
}

async function bossHealthBar(){
    if(dmgDealt > 0){
        bossHealth -= dmgDealt;
        document.getElementById('boss').src = "images/bossHat.gif";
        await sleep(500);
        //due to await sleep the animation may over write the death one, so here is a fix
        if(gameStatus != false){
            document.getElementById('boss').src = "images/boss.gif";
        }
        
    }

    document.getElementById("bossHealthBar").value = bossHealth;

    if(bossHealth <= 0){
        document.getElementById('boss').src = "images/bossDead.gif";
        victory();
    }
}

function calculateDmg(){
    dmgDealt = wpm;
    if(cheat!=true){
        //use floor, gives the player an advantage
        dmgTaken = Math.floor(amountOfErrors/2)+1;
    }
}

async function healthBar(){
    bossHealthBar();
    //ensures bossHealthBar()runs first
    await sleep(10);
    if(bossHealth > 0){
        playerHealthBar();
    }
}