//core functions of the game
var currentWord, amountOfErrors, wpm, previousWord, timer, time, tries, overallWpm;

$(document).ready(function() {
    //Check if enter or esc is pressed
    $(document).keydown(function(e) {
        if(e.which == 27){
            alert("game paused");
        }else if(inGame == false){
            //Start the game
            if(gameStatus == false){
                newGame();
            }
            startGame();
        }else if(e.which == 13){
            enterWord();
        }
    })
})

//simular feature as sleep, provided by Dan Dascalescu on stack overflow, unlike setTimeout this function is synchronous.
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function newGame(){
    //reset game visuals and audio
    stopAllButBossMusic();
    document.body.style.backgroundImage = "url('images/backgroundDim.jpg')"; 
    document.getElementById('boss').src = "images/boss.gif";

    //start the game, reset values
    gameStatus = true;
    playerHealth = 5;
    dmgTaken = 0;
    bossHealth = 100;
    dmgDealt = 0;
    tries = 0;
    overallWpm = 0;
    cheat = false;
    healthBar();

    //play the music
    startBossMusic();
}

function startGame() {
    //Initialize start
    inGame = true;
    document.getElementById("wordField").focus();

    //Reset all variables
    amountOfErrors = 0;
    corrections = 0;
    wpm = 0;
    $("#currentWord").html("");
    pickWord();
    
    //start the timer
    startTimer();

    //store overall tries to calculate overallwpm
    tries += 1;
}

async function stopGame () {

    //Stop the timer
    clearInterval(timer);
    $("#wordField").val("");

    //Set game to stopped
    inGame = false;

    //Clear the current word
    $("#currentWord").html("<strong>...</strong>");

    //Show player his results
    
    calculateDmg();
    alert("Round "+tries+" results:\n\nWords per minute: "+wpm+"\nAmount of errors: "+amountOfErrors+"\n\nDamage dealt: "+dmgDealt+"\nDamage taken: "+dmgTaken);
    healthBar();
    overallWpm += wpm;

}

function startTimer() {

    //Reset timer back to 60 seconds
    time = 60;

    //Every 100ms the timer gets updated
    timer = setInterval(function () {
        if(time <= 0){
            stopGame();
        }
        time = time +- 0.1;
        $("#timer").html("<strong>"+time.toFixed(2)+"</strong>")
    },100);

}

function enterWord () {
    //If player is not in game
    if (inGame == true){ 
        //Is in game
        checkWord();
    }
}

function checkWord () {
    var enteredWord = $("#wordField").val();

    //Check if the player typed the correct string
    if(enteredWord == currentWord){
        //if true
        fadeColor("wordField", "backgroundColor", "rgb(0,250,0)");
        $("#wordField").val("");
        pickWord();
        wpm+=1;
        previousWord = currentWord;

    }else{
        //if false
        amountOfErrors += 1;
        fadeColor("wordField", "backgroundColor", "rgb(250,0,0)");
    }
}
function pickWord () {

    //Generate a random word
    generateWords();

    //Check if the word is too long, lags to much if put in wordGen.js more testing needed, currently this works as intended but not clean enough.
    if(currentWord.length > 9){
        pickWord();
    }else{
        //Show the word
        $("#currentWord").html("<strong>"+currentWord+"</strong>");
    }
}

//used for indicaton of correct or not
function fadeColor(id, property, color) {
    //save the orignal color
    var oProperty = "rgb(0,0,0)";

    $("#"+id+"").css(property, color);
    //restore after 200ms
    setTimeout(function() {
        $("#"+id+"").css(property, oProperty);
    },150);
}

function score(){
    return Math.round(overallWpm/tries);
}

async function gameOver(){
    //reset game state
    gameStatus = false;
    document.getElementById('boss').src = "images/youDied.gif";
    stopBossMusic();
    startDeathMusic();
    await sleep(6000);
    alert("You died!\nWith a wpm of "+score()+"\nThe boss has "+ bossHealth +" health left\nTry harder next time.");
};

async function victory(){
    //reset game state
    gameStatus = false;
    stopBossMusic();
    startVictoryMusic();
    document.body.style.backgroundImage = "url('images/background.jpg')"; 
    await sleep(1);
    alert("You win!\nWith a wpm of "+score());
    playerName = prompt("Entre your name (no spaces allowed in name)");
    while(playerName.indexOf(' ') >= 0 || playerName == ""){
        alert("no spaces allowed in name");
        playerName = prompt("Entre your name (no spaces allowed in name)");
    }
    if(cheat == true){
        playerName = "BIG_BABY_"+playerName;
        alert("What?\nYou want to be on the leaderboard for being a baby?")
        alert("...")
        alert("fine")
    }

    storeLocal(playerName,score());
    scoreBoard();
}