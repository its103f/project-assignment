//audio controls
var muted = localStorage.getItem("muted");
var bossMusic = document.getElementById("bossMusic");
var deathMusic = document.getElementById("deathMusic");
var victoryMusic = document.getElementById("victoryMusic");

$(document).ready(function() {
    checkMute();
    $("#mute").click(function(){
        muteSwitch();
    })
})

function checkMute(){
    if(muted == "true"){
        //disable all audio sources
        bossMusic.muted = true;
        deathMusic.muted = true;
        victoryMusic.muted = true;
    }else{        
        bossMusic.muted = false;
        deathMusic.muted = false;
        victoryMusic.muted = false;
    }
}

function muteSwitch(state){
    if(muted == "false"){
        //disable all audio sources
        bossMusic.muted = true;
        deathMusic.muted = true;
        victoryMusic.muted = true;
        localStorage.setItem("muted","true");
        muted = localStorage.getItem("muted");


    }else{
        bossMusic.muted = false;
        deathMusic.muted = false;
        victoryMusic.muted = false;
        localStorage.setItem("muted","false");
        muted = localStorage.getItem("muted");
    }
}

function startBossMusic(){
    bossMusic.loop = true;
    bossMusic.load();
    bossMusic.play();
    bossMusic.volume = 0.1; 
}
function stopBossMusic(){
    bossMusic.pause();
    bossMusic.src = bossMusic.src;
}

function startVictoryMusic(){
    victoryMusic.loop = true;
    victoryMusic.load();
    victoryMusic.play();
    victoryMusic.volume = 0.1;
}
function stopVictoryMusic(){
    victoryMusic.pause();
    victoryMusic.src = victoryMusic.src;
}

function startDeathMusic(){
    deathMusic.load();
    deathMusic.play();
    deathMusic.volume = 0.1;
}
function stopDeathMusic(){
    deathMusic.pause();
    deathMusic.src = deathMusic.src;
}

function stopAllButBossMusic(){
    stopDeathMusic();
    stopVictoryMusic();
}